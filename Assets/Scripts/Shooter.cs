﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    const int SphereballFrequency = 3;

    int sampleBallCount;


    public GameObject[] ballPrefabs;
    public GameObject ballHolder;
    public GameObject ballPrefab;
    public float shotSpeed;
    public float shotTorque;
    public float baseWidth;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1")) Shot();
    }

    //ボールプレファブからランダムに1つ選ぶ
    GameObject SampleBall()
    {
        GameObject prefab = null;

        //特定の回数に一回ボールを選択する
        if(sampleBallCount % SphereballFrequency == 0)
        {
            int index = Random.Range(0, ballPrefabs.Length);
            prefab = ballPrefabs[index];
        }
        else
        {
            int index = Random.Range(0, ballPrefabs.Length);
            prefab = ballPrefabs[index];
        }

        sampleBallCount++;

        return prefab;
    }

    Vector3 GetInstantiatePosition()
    {
        //画面サイズとInputの割合からボール生成のポジションを計算
        float x = baseWidth * (Input.mousePosition.x / Screen.width) - (baseWidth / 2);
        float y = baseWidth * (Input.mousePosition.y / Screen.width) - (baseWidth / 2);
        
        return transform.position + new Vector3(x, y, 0);
    }

    public void Shot()
    {

        //プレファブからCandyオブジェクトを生成
        GameObject ball = (GameObject)Instantiate
                (
            SampleBall(),
            GetInstantiatePosition(),
            Quaternion.identity
       );

        //CnandyオフジェクトのRigidbodyを取得し力と回転を加える
        Rigidbody ballRigidbody = ball.GetComponent<Rigidbody>();
        ballRigidbody.AddForce(transform.forward * shotSpeed);
        ballRigidbody.AddTorque(new Vector3(0, shotTorque, 0));

    }
}
