﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectDestroyer : MonoBehaviour
{
    int score;

    public Text ScoreLabel;

    //public void Update()
    //{
    //    //スコアラベルを更新
    //    int score = CalcScore();
    //    ScoreLabel.text = "Score :" + score + "point";
    //}

    int CalcScore()
    {
        return (int)score;
    }

    private void OnTriggerEnter(Collider other)
    {
       //if(other.gameObject.tag == "Ball")
       // {
       //     //オブジェクトを削除
       //     Destroy(other.gameObject);
       // }

        if (other.gameObject.tag == "RedCylinder")
        {
            score += 20;
        }
        ScoreLabel.text = "Score :" + score + "point";
        Destroy(other.gameObject);
       
    }



  
}
